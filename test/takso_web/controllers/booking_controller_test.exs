defmodule TaksoWeb.BookingControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, Sales.Taxi}
  alias Takso.Guardian
  alias Takso.Accounts.User

  import Ecto.Query, only: [from: 2]

  setup do
    user = Repo.insert!(%User{username: "fred", name: "Fred", password: "parool"})
    conn = build_conn()
           |> bypass_through(Takso.Router, [:browser, :browser_auth, :ensure_auth])
           |> get("/")
           |> Map.update!(:state, fn (_) -> :set end)
           |> Guardian.Plug.sign_in(user)
           |> send_resp(200, "Flush the session")
           |> recycle
    {:ok, conn: conn}
  end

# The following tests will fail if you don't provide your Key for Bing Maps Services
  # See File lib\takso_web\services\geolocation.ex (line 21)
  '''
  test "Booking Acceptance by Shortest Distance", %{conn: conn} do
    Repo.insert!(%Taxi{status: "available", location: "Muuseumi tee 2, 60534, Tartu"})
    Repo.insert!(%Taxi{status: "available", location: "Narva maantee 18, 51009, Tartu"})

    query = from t in Taxi, where: t.status == "available", select: t
    [t1, _] = Repo.all(query)

    assert t1.location == "Muuseumi tee 2, 60534, Tartu"

    conn = post conn, "/bookings", %{booking: [pickup_address: "Juhan Liivi 2, 50409, Tartu", dropoff_address: "Muuseumi tee 2, 60534, Tartu"]}
    conn = get conn, redirected_to(conn)

    response = html_response(conn, 200)
    matches = Regex.named_captures(~r/Your taxi will arrive in (?<dur>\d+) minutes/, response)

    [t2] = Repo.all(query)

    assert matches["dur"] == "9"
    assert t2.location == "Muuseumi tee 2, 60534, Tartu"
  end

  test "Booking Acceptance", %{conn: conn} do
    Repo.insert!(%Taxi{status: "available", location: "Narva maantee 27, 51009, Tartu"})
    conn = post conn, "/bookings", %{booking: [pickup_address: "Juhan Liivi 2, 50409, Tartu", dropoff_address: "Muuseumi tee 2, 60534, Tartu"]}
    conn = get conn, redirected_to(conn)
    assert html_response(conn, 200) =~ ~r/Your taxi will arrive in \d+ minutes/
  end
  '''

  test "Booking Rejection", %{conn: conn} do
    Repo.insert!(%Taxi{status: "busy", location: "Narva maantee 27, 51009, Tartu"})
    conn = post conn, "/bookings", %{booking: [pickup_address: "Juhan Liivi 2, 50409, Tartu", dropoff_address: "Muuseumi tee 2, 60534, Tartu"]}
    conn = get conn, redirected_to(conn)
    assert html_response(conn, 200) =~ ~r/At present, there is no taxi available!/
  end






end
