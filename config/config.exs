# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :takso,
  ecto_repos: [Takso.Repo]

# Configures the endpoint
config :takso, TaksoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wXjMqrXGpJGrhCc8H9PG2hQrdvURWlJ52DW4p6TeiYxv3KaRT5Vml4MEDuCBsalC",
  render_errors: [view: TaksoWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Takso.PubSub,
  live_view: [signing_salt: "yk4CWk1z"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :takso, Takso.Guardian,
  issuer: "takso",
  secret_key: "5ZsZcgL+IJfnZC9+dEwrO6fgxltBxfX7757bY61NzV5onhHd+eCGtzgSIgPLzJ3+" # put the result of running `mix guardian.gen.secret`
